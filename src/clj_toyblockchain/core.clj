(ns clj-toyblockchain.core
  (:require [clj-toyblockchain.server :as server]
            [clj-toyblockchain.client :as client]))

(server/run-server)
