(ns clj-toyblockchain.bc
  (:require [digest]
            [clj-time.local :as l]))

(defrecord Block [index timestamp transactions proof prev-hash])

(defrecord Blockchain [chain transactions nodes])

(defrecord Transaction [sender recipient amount])

(defn hash-block [cur-block]
  (let [cur-contents (map #(% cur-block) (keys cur-block))]
    (digest/sha-256 (reduce str cur-contents))))

(defn valid-proof? [last-proof proof]
  (let [cur-str (reduce str [last-proof proof])]
    (= (subs (digest/sha-256 cur-str) 0 4) "0000")))

(defn proof-of-work [last-proof]
  ;; taken from learn blockchains by building one from hackernoon
  ;; find number p st hash('last-proof' + 'p') creates a hash with 4 leading zeros
  (let [p (atom 0)]
    (while (not (valid-proof? last-proof @p))
      (swap! p inc))
    @p)
  )

(defn new-block [prev-block tx]
  (let [ts (l/local-now) 
        prev-hash (hash-block prev-block)
        prev-proof (:proof prev-block)
        cur-proof (proof-of-work prev-proof)
        cur-idx (inc (:index prev-block))]
    (->Block cur-idx ts tx cur-proof prev-hash)))


(defn mine-block [prev-block txs node-id]
  ;;returns new block to add to blockchain
  ;;sender is "0" meaning new coint, recipient is current node, amt is 1
  (let [new-tx (Transaction. "0" node-id 1)
        cur-txs (conj txs new-tx)]
    (new-block prev-block cur-txs)))


(defn genesis-block []
  (let [ts (l/local-now)
        idx 0
        tx []
        cur-proof 100
        prev-hash "1"]
    (->Block idx ts tx cur-proof prev-hash)))

(defn new-chain []
  (let [first-block (genesis-block)]
    (->Blockchain [first-block] [] #{})))


;; there's gotta be a lispy way of doing this...
(defn valid-chain? [chain]
  ;; range starts at 1 to start after genesis block
  (let [is-valid (atom true)
        i (atom 1)
        chain-len (count chain)]
    (while (and @is-valid (< i chain-len))
      (let [cur-block (get chain i)
            prev-block (get chain (- i 1))
            prev-proof (:proof prev-block)
            cur-proof (:proof cur-block)
            prev-hash (:prev-hash cur-block)
            hashed-prev (hash-block prev-block)
            correct-proof (valid-proof? prev-proof cur-proof)
            correct-hash (= hashed-prev prev-hash)
            cur-valid (and correct-proof correct-hash)]
        (swap! is-valid #(and % cur-valid))
        (swap! i inc))
      )
    @is-valid))
              
  
