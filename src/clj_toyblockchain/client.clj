(ns clj-toyblockchain.client
  (:require [org.httpkit.client :as http]
            [clojure.data.json :as json]
            [clj-toyblockchain.bc :as bc])
  )

;;synchronous with promise
(defn get-node-chain [node]
    (let [opts {:timeout 10000
              :user-agent "clojure-toyblockchain-agent"
              :headers {"Accept" "application/json"}}
          {:keys [status headers body error] :as resp} @(http/get node opts)]
      (if error
        {:chain [] :length 0}
        (json/read-str body :key-fn keyword))
      ))

;;async with callback
(defn send-transaction [sender receiver amount]
  (let [opts {:timeout 10000
              :user-agent "clojure-toyblockchain-agent"
              :query-params {:from sender :to receiver :amount amount}
              :headers {"Content-type" "application/json"}}]
    ;; think of ways to make generic?
    (http/post "http://localhost:3000/tx" opts
               (fn [{:keys [status headers body error]}]
                 (if error
                   (println "transaction failed: " error)
                   (println "transaction: " (json/read-str body)))))
    ))
  


(defn get-node-chains [nodes]
  ;; assume node addrss format  already has http:// (need to write cleanser)
  (map get-node-chain nodes))

(defn filter-chain [my-chain-length other-chain-struct]
  ;; other-chain-struct is composed of :chain and :length as per the json response of addr/chain
  (let [other-chain-length (:length other-chain-struct)
        other-chain (:chain other-chain-struct)]
    (and (> other-chain-length my-chain-length) (bc/valid-chain? other-chain))))
    

;; pick longest valid chain from all nodes, replace field indicates if our chain isn't the best and should be replaced
(defn consensus-fn [chain nodes]
  ;;chain-record has chain, transactions, and nodes
  (let [chain-length (count chain)
        other-chains (get-node-chains nodes)
        filtered-others (filter filter-chain other-chains)
        ]
    (if (not (empty? filtered-others))
      (let [best-chain (first (sort-by :length > filtered-others))]
        {:chain best-chain :length (:length best-chain) :replace true})
      {:chain chain :length chain-length :replace false})))



    

