(ns clj-toyblockchain.server
  (:require [compojure.core :as cj]
            [compojure.route :as route]
            [compojure.handler :as handler]
            [ring.middleware.json :as mw]
            [ring.adapter.jetty :as jetty]
            [ring.middleware.reload :refer [wrap-reload]]
            [clj-toyblockchain.bc :as bc]
            [clj-toyblockchain.state :as bcst]
            [clj-toyblockchain.client :as client]
            ))

(def not-nil? (complement nil?))

(defonce node-id (str (java.util.UUID/randomUUID)))
  

(cj/defroutes app-routes
  (cj/POST "/tx" request
        (let [sender (or (get-in request [:params :from])
                         (get-in request [:body :from]))
              recipient (or (get-in request [:params :to])
                            (get-in request [:body :to]))
              amt (or (get-in request [:params :amount])
                      (get-in request [:body :amount]))
              new-tx (bc/->Transaction sender recipient amt)
              ]
          (bcst/add-transaction new-tx)
          {:status 201
           :body {:recipient recipient :amount amt}}))
  (cj/GET "/mine" request
          (let [prev-block (bcst/get-last-block)
                txs (bcst/get-transactions)
                mined (bc/mine-block prev-block txs node-id)
                added-txs (:transactions mined)
                added-idx (:index mined)
                added-proof (:proof mined)
                added-prev-hash (:prev-hash mined)]
            ;;since new block is forged, clear transactions
            (bcst/clear-transactions)
            {:status 201
             :body {:message "new block forged"
                    :index added-idx
                    :transactions added-txs
                    :proof added-proof
                    :prev-hash added-prev-hash}}
            ))
  (cj/GET "/chain" []
          (let [chain (bcst/get-chain)
                chain-length (count chain)]
            {:status 200
             :body {:chain chain
                    :length chain-length}}
            ))
  (cj/context "/nodes" []
              (cj/POST "/register" request
                       (let [node-addr-coll (or (get-in request [:params :nodes])
                                                (get-in request [:body :nodes]))]
                         (map #(bcst/register-node (str %)) node-addr-coll)
                         {:status 201
                          :body {:message "new nodes added. listing total nodes"
                                 :nodes (bcst/get-nodes)}}
                         ))
              (cj/GET "/resolve" []
                      (let [resolved-chain (client/consensus-fn (bcst/get-chain) (bcst/get-nodes))
                            replace? (:replace resolved-chain)
                            new-chain (:chain resolved-chain)]
                        (if replace?
                          (do (bcst/set-chain new-chain)
                              {:status 201
                               :body {:message "chain replaced"
                                      :chain new-chain}})
                          {:status 200
                           :body {:message "original chain intact"}}))
                      )
                          
              )
              
  
  (route/resources "/")
  (route/not-found "404 not found")
  )

  
(def app
  (-> (wrap-reload #'app-routes)
      (mw/wrap-json-body {:keywords? true :bigdecimals? true})
      (mw/wrap-json-response))
)

(defn run-server [& port]
  (jetty/run-jetty app {:port (or (first port) 3000)}))
          
