(ns clj-toyblockchain.state
  (:require [clj-toyblockchain.bc :as bc])
  (:import [clj_toyblockchain.bc Block]
           [clj_toyblockchain.bc Blockchain]
           [clj_toyblockchain.bc Transaction])
  )

(defn valid-blockchain? [cur-bc]
  (and (every? #(instance? Block %) (:chain cur-bc))
       (every? #(instance? Transaction %) (:transactions cur-bc))
       (every? #(string? %) (:nodes cur-bc))
       ))

(defonce cur-blockchain (atom (bc/new-chain) :validator valid-blockchain?))

(defn get-last-block []
  (last (:chain @cur-blockchain)))

(defn get-transactions []
  (:transactions @cur-blockchain))

(defn add-transaction [tx-to-add]
  (swap! cur-blockchain update-in [:transactions] conj tx-to-add))


(defn valid-node? [node-str]
  (not (nil? (re-seq #"\d+\.\d+\.\d+\.\d+:\d+" node-str))))

(defn node-cleanse [node-str]
  (if (nil? (re-seq #"^http(?:s?)://" node-str))
    (str "http://" node-str) node-str))

(defn register-node [new-node]
  (let [cleansed (node-cleanse new-node)]
    (when (valid-node? cleansed)
      (swap! cur-blockchain update-in [:nodes] conj new-node))))

(defn get-nodes []
  (:nodes @cur-blockchain))

(defn get-chain []
  (:chain @cur-blockchain))

(defn get-chain-length []
  (count (get-chain)))

(defn set-chain [new-chain]
  (swap! cur-blockchain assoc-in [:chain] new-chain))

(defn set-nodes [new-nodes]
  (swap! cur-blockchain assoc-in [:nodes] new-nodes))

(defn clear-transactions []
  (swap! cur-blockchain assoc-in [:transactions] []))
