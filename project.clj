(defproject clj-toyblockchain "0.1.0-SNAPSHOT"
  :description "toy blockchain written in clojure"
  :url "https://gitlab.com/derekxkwan.com/clj-toyblockchain"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/data.json "0.2.6"]
                 [digest "1.4.8"]
                 [clj-time "0.14.4"]
                 [compojure "1.6.1"]
                 [ring/ring-json "0.4.0"]
                 [ring/ring-core "1.7.0-RC1"]
                 [ring/ring-jetty-adapter "1.7.0-RC1"]
                 [ring/ring-devel "1.7.0-RC1"]
                 [http-kit "2.3.0"]
                 ]
  :plugins [[lein-ring "0.9.7"]]
  :profiles
  {:dev {:dependencies [[ring/ring-mock "0.3.0"]]}}
  :repl-options {:color false}
  :main clj-toyblockchain.core
  )
