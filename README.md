# clj-toyblockchain

toy blockchain in clojure to learn about blockchain and get comfier with clojure. Uses `compojure` for routing, `ring` for serving, `http-kit` for clienting(?), `clj-time` for timestamps, and `digest` for the sha256 implementation. 


follows [Learn Blockchains by Build One](https://hackernoon.com/learn-blockchains-by-building-one-117428612f46) by Hacker Noon

## Usage

type `lein run` (requires `leiningen`) to start server running on port 3000. Use HTTP client such as cURL to interact with server.

## Sitemap

- `/mine` (GET) - mine a new block, adding queued transactions
- `/tx` (POST) - create new transactions, included in next mined block
- `/chain` (GET) - return stored chain in JSON format
- `/nodes/register` (POST) - register new nodes from JSON of form `{"nodes":["nodeip:port", "nodeip:port", "nodeip:port"]}`
- `/nodes/resolve` (GET) - run consensus function on all chains of given nodes and resolve local chain with the longest valid chain


## License

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
